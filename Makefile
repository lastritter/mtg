

all : 
	cd src && make
	cd test && make
	cd gestures && make


clean : 
	cd src && make clean
	cd test && make clean
	cd gestures && make clean

